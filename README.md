# Cassini

Les cartes de Cassini pour tous 


TUTORIEL UTILISATEURS

NB : Une version PDF du tutoriel utilisateurs, plus lisible, est disponible.

Il y a deux types d’utilisateurs : ceux qui entrent directement leur requête URL et ceux qui vont explorer les toponymes en France à l’aide de l’interface de visualisation cartographique CartoGraphAPI.

I.	Recherche de toponymes en France par URL

    A.	Requêtes
    
        1.	Simple visualisation des ressources
        
Cette requête permet de voir un extrait des données :
*http://geohistoricaldata.org/data/collections/place/items*

        2.	Recherche d’un toponyme à partir de son identifiant id
        
Les toponymes sont identifiés chacun par un seul et unique identifiant. Cette requête renvoie l’URI du toponyme :
*http://geohistoricaldata.org/data/collections/place/items?id*

        3.	Recherche de toponymes à partir de paramètres
        
Les différents paramètres d’une requête sont : les filtres (pour faire une recherche en connaissant un ou plusieurs critères suivants : le nom, la nature, le statut), la bbox (zone rectangulaire), la zone circulaire autour d’un point et le choix du nombre de toponymes renvoyés.
            
            a)	Recherche de toponymes à partir d’un seul paramètre
            
                (1)	Recherche de toponymes à partir d’un filtre
                
**Respecter la casse dans les critères du filtre**

                    (a)	Recherche à partir d’un seul critère
                    
                        (i)	Recherche d’un toponyme à partir d’une chaîne de caractères contenue dans son nom
                        
Exemple : Recherche d’un toponyme dont le nom commence par « Par » :
*http://geohistoricaldata.org/data/collections/place/items?filter=”(name=’Par’)”*

                        (ii)	Recherche d’un toponyme à partir de sa nature
                        
Exemple : Recherche d’un toponyme dont la nature est « Clocher » :
*http://geohistoricaldata.org/data/collections/place/items?filter=”(nature=’Clocher’)”*

Les natures possibles sont : Abbaye, Bourg, Cabane, Chapelle, Château, Cimetière, Clocher, Commanderie, CorpsDeGarde, Croix, Fort, Gentilhommière, Hameau, Justice, Maison, MoulinÀEau, MoulinÀVent, Port, Prieuré, Redoute, Ville.
                        
                        (iii)	Recherche d’un toponyme à partir de son statut
                        
Exemple : Recherche d’un toponyme dont le statut est « ChefLieuCommunal » :
*http://geohistoricaldata.org/data/collections/place/items?filter=”(status=’ChefLieuCommunal’)”*

Les natures possibles sont : ChefLieuCommunal, ChefLieuParoissialAncienRegime.

                    (b)	Recherche à partir de plusieurs critères
                    
Exemple : Recherche d’un toponyme dont la nature est « Clocher » et dont le statut est « ChefLieuCommunal » :
*http://geohistoricaldata.org/data/collections/place/items?filter=”(nature=’Clocher’)AND(status=’ChefLieuCommunal’)”*
                
                (2)	Recherche de toponymes dans une zone circulaire autour d’un point
                
**Il faut connaître les coordonnées géographiques minimales et maximales en latitude et longitude.**

Exemple : Recherche d’un toponyme dans une zone circulaire autour du point de coordonnées (2.35, 48.84), 300 km ou moins :
*http://geohistoricaldata.org/data/collections/place/items?filter=”DWITHIN(POINT(2.35,48.84),300)”*
                
                (3)	Recherche de toponymes à partir d’une bbox
                
**Il faut connaître les coordonnées géographiques minimales et maximales en latitude et longitude.**
Par exemple, pour un rectangle dont on a l’emprise suivante (pour plus d'informations, cf le document TutorielUtilisateurs.pdf dans le même dossier) :

        	Minimale	Maximale
        	
Latitude	43.9	    44.0

Longitude	5.7     	5.8

*http://geohistoricaldata.org/data/collections/place/items?bbox=5.7,43.9,5.8,44.0*

                (4)	Ajout du choix du nombre de toponymes renvoyés
                
Par défaut, dix toponymes sont renvoyés. Si vous en souhaitez plus (dans la limite du nombre de toponymes trouvés) ou moins, il suffit de changer la limite, par exemple 5 :
*http://geohistoricaldata.org/data/collections/place/items?limit=5*
           
            b)	Recherche de toponymes à partir de plusieurs paramètres
           
**Dans tous les cas, il est possible de placer une limite**, à laquelle on peut rajouter d’autres paramètres : par exemple, il est possible de réaliser une requête avec un filtre (nom, nature, statut) et une zone circulaire autour d'un point (pour plus d'informations, cf le document TutorielUtilisateurs.pdf dans le même dossier).

Exemple : Recherche de cinq toponymes dont la nature est « Clocher » et dont le statut est « ChefLieuCommunal », situés dans une bbox :
*http://geohistoricaldata.org/data/collections/place/items?filter=”(nature=’Clocher’)AND(status=’ChefLieuCommunal’)”&bbox=5.7,43.9,5.8,44.0&limit=5*

    B.	Résultats des requêtes
    
Le résultat des requêtes est affiché dans le navigateur au format geojson.

    C.	Téléchargement des données
    
Il est possible de télécharger le résultat des requêtes pour pouvoir les afficher dans un logiciel de SIG. Le fichier s’enregistre dans le dossier « Téléchargement ».

II.	Exploration des toponymes en France à l’aide de l’interface de visualisation cartographique CartoGraphAPI

L’interface de visualisation cartographique CartoGraphAPI vous permet d’explorer des toponymes en France, de télécharger les données des toponymes qui vous intéressent ainsi que de visualiser une partie de ces données (nom, nature, statut et identifiant du toponyme sélectionné) sur la carte de Cassini.

    A.	Recherche de toponymes
    
        1.	Recherche d’un toponyme unique
        
Les toponymes sont identifiés chacun par un seul et unique identifiant : dans le champ de texte correspondant, entrez l’identifiant du toponyme recherché.

        2.	Recherche d’un ou plusieurs toponymes
        
**Respecter la casse**

            a)	Recherche à partir d’un seul paramètre
            
                (1)	Recherche à partir d’un seul critère
                
                    (a)	Recherche d’un toponyme à partir du nom ou d’une partie du nom
                    
Il est possible d’effectuer la recherche de toponymes à partir de leur nom ou d’une partie de leur nom. Pour ce faire, tapez le nom ou la partie du nom de toponymes recherchés dans le champ de texte correspondant.

                    (b)	Recherche d’un toponyme à partir de sa nature
                    
La sélection de la nature désirée se fait par le menu de sélection.

Les natures possibles sont : Abbaye, Bourg, Cabane, Chapelle, Château, Cimetière, Clocher, Commanderie, CorpsDeGarde, Croix, Fort, Gentilhommière, Hameau, Justice, Maison, MoulinÀEau, MoulinÀVent, Port, Prieuré, Redoute, Ville, anyplace (ce dernier terme sert si la nature du toponyme désiré vous est indifférente).

                    (c)	Recherche d’un toponyme à partir de son statut
                    
La sélection du statut désiré se fait par le menu de sélection.

Les statuts possibles sont : AnyStatus (ce terme sert si le statut du toponyme désiré vous est indifférent), ChefLieuCommunal, ChefLieuParoissialAncienRegime.
Parfois, un toponyme peut avoir plusieurs statuts (ces derniers ayant été modifiés au cours de l’Histoire).

                (2)	Recherche à partir de plusieurs critères
                
Il est possible d’effectuer la recherche de toponymes en utilisant plusieurs des critères précédemment présentés. Pour ce faire, simplement remplir plusieurs des champs de texte au lieu de n’en remplir qu’un seul.

                (3)	Recherche de toponymes dans une zone circulaire autour d’un point
                
**Il faut connaître les coordonnées géographiques (latitude et longitude) du point autour duquel on souhaite effectuer la recherche de toponymes.**

Il est possible d’effectuer la recherche de toponymes dans une zone circulaire autour d’un point. Pour ce faire, rentrez les coordonnées du point ainsi que la distance de recherche (en km) autour de ce point dans les champs de texte correspondants.

                (4)	Recherche de toponymes situés dans une zone rectangulaire
                
**Il faut connaître les coordonnées géographiques minimales et maximales en latitude et longitude.**

Par exemple, pour un rectangle dont on a l’emprise suivante (pour plus d'informations, cf le document TutorielUtilisateurs.pdf dans le même dossier) :

        	Minimale	Maximale
        	
Latitude	43.9	    44.0

Longitude	5.7     	5.8
On rentre les différentes coordonnées dans les champs de texte correspondants.

                (5)	Ajout du choix du nombre de toponymes renvoyés
                
Par défaut, l’interface de visualisation cartographique CartoGraphAPI vous renvoie dix toponymes. Si vous souhaitez plus (dans la limite du nombre de toponymes trouvés) ou moins de toponymes, il suffit d’entrer une valeur dans le champ de texte correspondant.

            b)	Recherche à partir de plusieurs paramètres
            
**Dans tous les cas, il est possible de placer une limite**, à laquelle on peut rajouter d’autres paramètres : par exemple, il est possible de réaliser une requête avec un filtre (nom, nature, statut) et une zone circulaire autour d'un point (pour plus d'informations, cf le document TutorielUtilisateurs.pdf dans le même dossier).

Exemple : Recherche de cinq toponymes dont la nature est « Clocher » et dont le statut est « ChefLieuCommunal », situés dans une bbox.

        3.	Lancement de la recherche de toponymes
        
Lorsque vous avez rempli tous les champs de texte que vous souhaitiez avec les informations dont vous disposiez, lancez la requête en validant les critères.

    B.	Téléchargement des données
    
Pour télécharger les données (au format geojson) et ainsi pouvoir éventuellement les afficher dans un logiciel de SIG, il suffit de cliquer sur le bouton « Download ». Le fichier geojson s’enregistre dans le dossier « Téléchargement ».

    C.	Visualisation de la carte avec les toponymes
    
Une fois que la recherche de toponymes est lancée, les toponymes s’affichent à l’aide de marqueurs sur la carte de Cassini. Vous pouvez visualiser une partie des données des toponymes (nom, nature, statut et identifiant du toponyme sélectionné) sur la carte.